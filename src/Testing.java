import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Testing {

	//variables
		//objeto_que_habla_con_el_navegador
		private WebDriver driver;
		
		
		//Declarar_URL
		private String baseurl = "http://qa-trainingw7:86/";
		
		@BeforeTest
		public void preconditions() {
			//Siempre_poner_esta_URL
			System.setProperty("webdriver.chrome.driver","C:\\Windows\\System32\\drivers\\chromedriver.exe");
			//abrir_el_navegador
			driver = new ChromeDriver();
			//Paso2_Ir_URL
			driver.get(baseurl);
			
			driver.manage().window().maximize();
			
			
		}//fin_metodo
		
		

		//-------------------------------------------------------------------------------------------------------------------
		
		@Test(description="LoginPage_Sucess: //TC2-Verify the user is able to login when sending valid credentials")
		public void test2() {
			
			
			WebElement pageLogin;
			WebElement userName;
			WebElement password;
			WebElement btonLogIn;
			
			
			WebElement titleHome;
			
			
			
			//hace_unaVariableParaQueGuardeLaVariableXpath
			String pageLoginXpath = "//*[@id=\"ctl00_LoginView_LoginLink\"]";
		
			
			String titleLogin_XPath= "//*[@id=\"col_main_right\"]/h2"; 
			String titleLogin_Obtein;
			String titleLogin_Expected = "Login";
			
			String userName_Xpath = "//*[@id=\"ctl00_Main_LoginConrol_UserName\"]";
			String password_Xpath = "//*[@id=\"ctl00_Main_LoginConrol_Password\"]";
			String btonLogIn_Xpath = "//*[@id=\"ctl00_Main_LoginConrol_LoginButton\"]";
			
			
			String titleHome_XPath = "//*[@id=\"col_main_right\"]/h3";
			String titleHome_Obtein;
			String titleHome_Expected = "Browse Categories";
			
		
					
			//EncontaraElementoParaCargarlo
			pageLogin = driver.findElement(By.xpath(pageLoginXpath));//absoluto
			pageLogin.click();//DarClickAlElemento
			waitSeconds (driver, 1);
			
			pageLogin = driver.findElement(By.xpath(titleLogin_XPath));//encuentra elemeto
			titleLogin_Obtein = pageLogin.getText();//para q obtenga el titulo
			//Asert=validar q cumpla la condicion
			Assert.assertEquals(titleLogin_Expected, titleLogin_Obtein);
			
			
			
			//Obtener el WebElement correspondinet al textfield username
			userName = driver.findElement(By.xpath(userName_Xpath));
			userName.click();
			//Ingresar valores en el textfield password
			userName.sendKeys("Maleon");
			waitSeconds (driver, 1);
			
			
			//Obtener el WebElement correspondinet al textfield pass
			password = driver.findElement(By.xpath(password_Xpath));
			password.click();
			//Ingresar valores en el textfield password
			password.sendKeys("Ml12345*");
			waitSeconds (driver, 1);
			
			

			//Obtener el WebElement boton login
			btonLogIn = driver.findElement(By.xpath(btonLogIn_Xpath));
			btonLogIn.click();
			//Ingresar valores en el textfield password
			waitSeconds (driver, 3);
			
			
			titleHome = driver.findElement(By.xpath(titleHome_XPath));//encuentra elemeto
			titleHome_Obtein = titleHome.getText();//para q obtenga el titulo
			//Asert=validar q cumpla la condicion
			Assert.assertEquals(titleHome_Expected, titleHome_Obtein);
			waitSeconds (driver, 2);
			
			
					
		}//fin_metodo
		
		
		
		
		@Test(description="RegisterPage_isDisplayed_correctly: //TC-11-Verify the \"Register\" page is displayed correctly when clicking on \"Register\" link")
		public void Test1() {
				
					

		WebElement registerTitle;
		WebElement registerPage;
					
		//hace_unaVariableParaQueGuardeLaVariableXpath
		String registerPage_Xpath = "//*[@id=\"ctl00_LoginView_RegisterLink\"]";
				
					
		String registerTitle_XPath= "//*[@id=\"col_main_right\"]/h2"; 
		String loginTitle_Obtein;
		String loginTitle_Expected = "Register";
		//EncontaraElementoParaCargarlo
							
		//EncontaraElementoParaCargarlo
		registerPage = driver.findElement(By.xpath(registerPage_Xpath));//absoluto
		registerPage.click();//DarClickAlElemento
					
		registerTitle = driver.findElement(By.xpath(registerTitle_XPath));//encuentra elemeto
		loginTitle_Obtein = registerTitle.getText();//para q obtenga el titulo
		//Asert=validar q cumpla la condicion
		Assert.assertEquals(loginTitle_Expected, loginTitle_Obtein);
							
		}//fin_metodo
		
		
		
		
		
			
			
			
			
			
//***********************************************************************************************************
			@AfterTest
			public void exit() {
				driver.quit();
			}//fin_metodo
			
		
			//Metodo para espera
			   public void waitSeconds(WebDriver driver, int seconds){
				      synchronized(driver){
				         try {
				            driver.wait(seconds * 1000);
				         } catch (InterruptedException e) {
				            // TODO Auto-generated catch block
				            //e.printStackTrace();
				         }
				      }
				   }
			   
}//FIN 
