import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Main {

	//variables
		//objeto_que_habla_con_el_navegador
		private WebDriver driver;
		
		
		//Declarar_URL
		private String baseurl = "http://qa-trainingw7:86/";
		
		@BeforeTest
		public void preconditions() {
			//Siempre_poner_esta_URL
			System.setProperty("webdriver.chrome.driver","C:\\Windows\\System32\\drivers\\chromedriver.exe");
			//abrir_el_navegador
			driver = new ChromeDriver();
			//Paso2_Ir_URL
			driver.get(baseurl);
			
			driver.manage().window().maximize();
			
			
		}//fin_metodo
		
		
		
		@Test(description="LoginPage_Displayed: //TC1-Verify the \"Login\" page is displayed correctly when clicking on \"Login\" link")
		public void Test1() {
			
			
			WebElement titleLogin;
			WebElement pageLogin;
			
			//hace_unaVariableParaQueGuardeLaVariableXpath
			String pageLoginXpath = "//*[@id=\"ctl00_LoginView_LoginLink\"]";
		
			
			String titleLogin_XPath= "//*[@id=\"col_main_right\"]/h2"; 
			String titleloLogin_Obtein;
			String titleLogin_Expected = "Login";
			//EncontaraElementoParaCargarlo
					
			//EncontaraElementoParaCargarlo
			pageLogin = driver.findElement(By.xpath(pageLoginXpath));//absoluto
			pageLogin.click();//DarClickAlElemento
			waitSeconds (driver, 3);
			
			titleLogin = driver.findElement(By.xpath(titleLogin_XPath));//encuentra elemeto
			titleloLogin_Obtein = titleLogin.getText();//para q obtenga el titulo
			//Asert=validar q cumpla la condicion
			Assert.assertEquals(titleLogin_Expected, titleloLogin_Obtein);
					
		}//fin_metodo
		
		//-------------------------------------------------------------------------------------------------------------------
		
		@Test(description="LoginPage_Sucess: //TC2-Verify the user is able to login when sending valid credentials")
		public void test2() {
			
			
			WebElement pageLogin;
			WebElement userName;
			WebElement password;
			WebElement btonLogIn;
			
			
			WebElement titleHome;
			
			
			
			//hace_unaVariableParaQueGuardeLaVariableXpath
			String pageLoginXpath = "//*[@id=\"ctl00_LoginView_LoginLink\"]";
		
			
			String titleLogin_XPath= "//*[@id=\"col_main_right\"]/h2"; 
			String titleLogin_Obtein;
			String titleLogin_Expected = "Login";
			
			String userName_Xpath = "//*[@id=\"ctl00_Main_LoginConrol_UserName\"]";
			String password_Xpath = "//*[@id=\"ctl00_Main_LoginConrol_Password\"]";
			String btonLogIn_Xpath = "//*[@id=\"ctl00_Main_LoginConrol_LoginButton\"]";
			
			
			String titleHome_XPath = "//*[@id=\"col_main_right\"]/h3";
			String titleHome_Obtein;
			String titleHome_Expected = "Browse Categories";
			
		
					
			//EncontaraElementoParaCargarlo
			pageLogin = driver.findElement(By.xpath(pageLoginXpath));//absoluto
			pageLogin.click();//DarClickAlElemento
			waitSeconds (driver, 1);
			
			pageLogin = driver.findElement(By.xpath(titleLogin_XPath));//encuentra elemeto
			titleLogin_Obtein = pageLogin.getText();//para q obtenga el titulo
			//Asert=validar q cumpla la condicion
			Assert.assertEquals(titleLogin_Expected, titleLogin_Obtein);
			
			
			
			//Obtener el WebElement correspondinet al textfield username
			userName = driver.findElement(By.xpath(userName_Xpath));
			userName.click();
			//Ingresar valores en el textfield password
			userName.sendKeys("Maleon");
			waitSeconds (driver, 1);
			
			
			//Obtener el WebElement correspondinet al textfield pass
			password = driver.findElement(By.xpath(password_Xpath));
			password.click();
			//Ingresar valores en el textfield password
			password.sendKeys("Ml12345*");
			waitSeconds (driver, 1);
			
			

			//Obtener el WebElement boton login
			btonLogIn = driver.findElement(By.xpath(btonLogIn_Xpath));
			btonLogIn.click();
			//Ingresar valores en el textfield password
			waitSeconds (driver, 3);
			
			
			titleHome = driver.findElement(By.xpath(titleHome_XPath));//encuentra elemeto
			titleHome_Obtein = titleHome.getText();//para q obtenga el titulo
			//Asert=validar q cumpla la condicion
			Assert.assertEquals(titleHome_Expected, titleHome_Obtein);
			waitSeconds (driver, 2);
			
			
					
		}//fin_metodo
		
		
		//-------------------------------------------------------------------------------------------------------------------
		
		@Test(description="LoginPage_Failed //TC3-Verify the user is not able to login when sending invalid credentials")
		public void Test3() {
			
			
			
			WebElement titlLogin;
			WebElement pagLogin;
			WebElement userName;
			WebElement password;
			WebElement btonLogIn;
				
			
			//hace_unaVariableParaQueGuardeLaVariableXpath
			String pagLoginXpath = "//*[@id=\"ctl00_LoginView_LoginLink\"]";
		
			
			String titLogin_XPath= "//*[@id=\"col_main_right\"]/h2"; 
			String titlloLogin_Obtein;
			String titlLogin_Expected = "Login";
			
			String userName_Xpath = "//*[@id=\"ctl00_Main_LoginConrol_UserName\"]";
			String password_Xpath = "//*[@id=\"ctl00_Main_LoginConrol_Password\"]";
			String btonLogIn_Xpath = "//*[@id=\"ctl00_Main_LoginConrol_LoginButton\"]";
			
			
					
			//EncontaraElementoParaCargarlo
			pagLogin = driver.findElement(By.xpath(pagLoginXpath));//absoluto
			pagLogin.click();//DarClickAlElemento
			waitSeconds (driver, 1);
			
			titlLogin = driver.findElement(By.xpath(titLogin_XPath));//encuentra elemeto
			titlloLogin_Obtein = titlLogin.getText();//para q obtenga el titulo
			//Asert=validar q cumpla la condicion
			Assert.assertEquals(titlLogin_Expected, titlloLogin_Obtein);
			
			
			
			//Obtener el WebElement correspondinet al textfield username
			userName = driver.findElement(By.xpath(userName_Xpath));
			userName.click();
			//Ingresar valores en el textfield password
			userName.sendKeys("Maleon");
			waitSeconds (driver, 1);
			
			
			//Obtener el WebElement correspondinet al textfield pass
			password = driver.findElement(By.xpath(password_Xpath));
			password.click();
			//Ingresar valores en el textfield password--INCORRECTOS
			password.sendKeys("Ml100005*");
			waitSeconds (driver, 1);
			
			

			//Obtener el WebElement boton login
			btonLogIn = driver.findElement(By.xpath(btonLogIn_Xpath));
			btonLogIn.click();
			//Ingresar valores en el textfield password
			waitSeconds (driver, 3);
			
			
			
			titlLogin = driver.findElement(By.xpath(titLogin_XPath));//encuentra elemeto
			titlloLogin_Obtein = titlLogin.getText();//para q obtenga el titulo
			//Asert=validar q cumpla la condicion
			Assert.assertEquals(titlLogin_Expected, titlloLogin_Obtein);
			waitSeconds (driver, 2);
			
			
			
					
		}//fin_metodo
		
		
		

		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
					

			@Test(description="RegisterPage_isDisplayed_correctly: //TC-11-Verify the \"Register\" page is displayed correctly when clicking on \"Register\" link")
			public void Test4() {
					
						

			WebElement registerTitle;
			WebElement registerPage;
						
			//hace_unaVariableParaQueGuardeLaVariableXpath
			String registerPage_Xpath = "//*[@id=\"ctl00_LoginView_RegisterLink\"]";
					
						
			String registerTitle_XPath= "//*[@id=\"col_main_right\"]/h2"; 
			String loginTitle_Obtein;
			String loginTitle_Expected = "Register";
			//EncontaraElementoParaCargarlo
								
			//EncontaraElementoParaCargarlo
			registerPage = driver.findElement(By.xpath(registerPage_Xpath));//absoluto
			registerPage.click();//DarClickAlElemento
						
			registerTitle = driver.findElement(By.xpath(registerTitle_XPath));//encuentra elemeto
			loginTitle_Obtein = registerTitle.getText();//para q obtenga el titulo
			//Asert=validar q cumpla la condicion
			Assert.assertEquals(loginTitle_Expected, loginTitle_Obtein);
								
			}//fin_metodo
				
					
			//-----------------------------------------------------		
			
			
			@Test(description="MandatoryMessages_areDisplayed_LeavingEmptyAll_MandatoryFields: "
					+ "//tc-12-Verify all the mandatory messages are displayed when leaving empty all the mandatory fields and clicking on \"Submit\" button")
			public void  Test5() {
			
				

				WebElement registerTitle;
				WebElement registerPage;
				
				WebElement first_Name;
				WebElement lastName;	
				WebElement email;
				WebElement userName;
				WebElement password;
				WebElement confirmPassword;
				WebElement securityQuestion;
				WebElement securityAnswer;
				WebElement buttonSubmit;
				
					
				
				//hace_unaVariableParaQueGuardeLaVariableXpath
				String registerPage_Xpath = "//*[@id=\"ctl00_LoginView_RegisterLink\"]";
				String first_Name_Selector = "#ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_FirstName";
				String lastName_XPath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_LastName\"]";
				String email_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Email\"]"; 
				String userName_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_UserName\"]";
				String password_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Password\"]";
				String confirmPassword_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_ConfirmPassword\"]";
				String securityQuestion_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Question\"]";
				String securityAnswer_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Answer\"]";
				String buttonSubmit_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl___CustomNav0_StepNextButtonButton\"]";
				
				String registerTitle_XPath= "//*[@id=\"col_main_right\"]/h2"; 
				String loginTitle_Obtein;
				String loginTitle_Expected = "Register";
				
						
				//EncontaraElementoParaCargarlo
				registerPage = driver.findElement(By.xpath(registerPage_Xpath));
				registerPage.click();//DarClickAlElemento
				
				registerTitle = driver.findElement(By.xpath(registerTitle_XPath));//encuentra elemeto
				loginTitle_Obtein = registerTitle.getText();//para q obtenga el titulo
				//Asert=validar q cumpla la condicion
				Assert.assertEquals(loginTitle_Expected, loginTitle_Obtein);
				
				
			
				//Obtener el WebElement correspondinet al textfield firstname
				first_Name = driver.findElement(By.cssSelector(first_Name_Selector));
				first_Name.click();
				//Ingresar valores en el textfield firstname
				first_Name.sendKeys("Martha");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield lasttname
				lastName = driver.findElement(By.xpath(lastName_XPath));
				lastName.click();
				//Ingresar valores en el textfield firstname
				lastName.sendKeys(" ");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield email
				email = driver.findElement(By.xpath(email_Xpath));
				email.click();
				//Ingresar valores en el textfield email
				email.sendKeys("ma1@gmail.com");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield username
				userName = driver.findElement(By.xpath(userName_Xpath));
				userName.click();
				//Ingresar valores en el textfield username
				userName.sendKeys("Maleon");
				waitSeconds (driver, 1);
				
				
				//Obtener el WebElement correspondinet al textfield password
				password = driver.findElement(By.xpath(password_Xpath));
				password.click();
				//Ingresar valores en el textfield password
				password.sendKeys("Ml12345*");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield confirm password
				confirmPassword = driver.findElement(By.xpath(confirmPassword_Xpath));
				confirmPassword.click();
				//Ingresar valores en el textfield confirm pasword
				confirmPassword.sendKeys("Ml12345*");
				waitSeconds (driver, 1);
				
				
				//Obtener el WebElement correspondinet al textfield security question
				securityQuestion = driver.findElement(By.xpath(securityQuestion_Xpath));
				securityQuestion.click();
				//Ingresar valores en el textfield confirm pasword
				securityQuestion.sendKeys("nombre mascota");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield security ansuer
				securityAnswer = driver.findElement(By.xpath(securityAnswer_Xpath));
				securityAnswer.click();
				//Ingresar valores en el textfield confirm pasword
				securityAnswer.sendKeys("kyra");
				waitSeconds (driver, 1);
				

				//Obtener el WebElement correspondinet al boton submit
				buttonSubmit = driver.findElement(By.xpath(buttonSubmit_Xpath));
				buttonSubmit.click();
				waitSeconds (driver, 3);
				
				
			}//fin_metodo
	
	
	//***************************************************************************************************************************
		
			@Test (description ="errorMessage_isDisplayed_createUsername_ThatIsAlreadyExist:"
					+ "//tc-13-Verify an error message is displayed when creating an user with an user name that is already taken")
			
			public void Test6() {
			
				WebElement registerTitle;
				WebElement registerPage;
				
				WebElement first_Name;
				WebElement lastName;	
				WebElement email;
				WebElement userName;
				WebElement password;
				WebElement confirmPassword;
				WebElement securityQuestion;
				WebElement securityAnswer;
				WebElement buttonSubmit;
				
					
				
				//hace_unaVariableParaQueGuardeLaVariableXpath
				String registerPage_Xpath = "//*[@id=\"ctl00_LoginView_RegisterLink\"]";
				String first_Name_Selector = "#ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_FirstName";
				
				String lastName_XPath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_LastName\"]";
				String email_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Email\"]"; 
				String userName_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_UserName\"]";
				String password_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Password\"]";
				String confirmPassword_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_ConfirmPassword\"]";
				String securityQuestion_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Question\"]";
				String securityAnswer_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Answer\"]";
				String buttonSubmit_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl___CustomNav0_StepNextButtonButton\"]";
				
				String registerTitle_XPath= "//*[@id=\"col_main_right\"]/h2"; 
				String loginTitle_Obtein;
				String loginTitle_Expected = "Register";
				
						
				//EncontaraElementoParaCargarlo
				registerPage = driver.findElement(By.xpath(registerPage_Xpath));
				registerPage.click();//DarClickAlElemento
				
				registerTitle = driver.findElement(By.xpath(registerTitle_XPath));//encuentra elemeto
				loginTitle_Obtein = registerTitle.getText();//para q obtenga el titulo
				//Asert=validar q cumpla la condicion
				Assert.assertEquals(loginTitle_Expected, loginTitle_Obtein);
				
				
			
				//Obtener el WebElement correspondinet al textfield firstname
				first_Name = driver.findElement(By.cssSelector(first_Name_Selector));
				first_Name.click();
				//Ingresar valores en el textfield firstname
				first_Name.sendKeys("Martha");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield lasttname
				lastName = driver.findElement(By.xpath(lastName_XPath));
				lastName.click();
				//Ingresar valores en el textfield firstname
				lastName.sendKeys("leon");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield email
				email = driver.findElement(By.xpath(email_Xpath));
				email.click();
				//Ingresar valores en el textfield email
				email.sendKeys("ma1@gmail.com");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield username
				userName = driver.findElement(By.xpath(userName_Xpath));
				userName.click();
				//Ingresar valores en el textfield username
				userName.sendKeys("Maleon");
				waitSeconds (driver, 1);
				
				
				//Obtener el WebElement correspondinet al textfield password
				password = driver.findElement(By.xpath(password_Xpath));
				password.click();
				//Ingresar valores en el textfield password
				password.sendKeys("Ml12345*");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield confirm password
				confirmPassword = driver.findElement(By.xpath(confirmPassword_Xpath));
				confirmPassword.click();
				//Ingresar valores en el textfield confirm pasword
				confirmPassword.sendKeys("Ml12345*");
				waitSeconds (driver, 1);
				
				
				//Obtener el WebElement correspondinet al textfield security question
				securityQuestion = driver.findElement(By.xpath(securityQuestion_Xpath));
				securityQuestion.click();
				//Ingresar valores en el textfield confirm pasword
				securityQuestion.sendKeys("nombre mascota");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield security ansuer
				securityAnswer = driver.findElement(By.xpath(securityAnswer_Xpath));
				securityAnswer.click();
				//Ingresar valores en el textfield confirm pasword
				securityAnswer.sendKeys("kyra");
				waitSeconds (driver, 1);
				

				//Obtener el WebElement correspondinet al boton submit
				buttonSubmit = driver.findElement(By.xpath(buttonSubmit_Xpath));
				buttonSubmit.click();
				waitSeconds (driver, 3);
				
				
			}//fin_metodo
	
	
			//------------------------------------------------------------------------------------------------------
			

			@Test (description ="ErrorMessage_IsDisplayed_sendDifferentPassword:"
					+ " //tc-14-Verify an error message is displayed when sending a different password on 'Password' and 'Confirm Password' fields")
			
			public void Test7() {
			
				
				WebElement registerTitle;
				WebElement registerPage;
				
				WebElement first_Name;
				WebElement lastName;	
				WebElement email;
				WebElement userName;
				WebElement password;
				WebElement confirmPassword;
				WebElement securityQuestion;
				WebElement securityAnswer;
				WebElement buttonSubmit;
				
					
				
				//hace_unaVariableParaQueGuardeLaVariableXpath
				String registerPage_Xpath = "//*[@id=\"ctl00_LoginView_RegisterLink\"]";
				String first_Name_Selector = "#ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_FirstName";
				
				String lastName_XPath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_LastName\"]";
				String email_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Email\"]"; 
				String userName_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_UserName\"]";
				String password_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Password\"]";
				String confirmPassword_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_ConfirmPassword\"]";
				String securityQuestion_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Question\"]";
				String securityAnswer_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Answer\"]";
				String buttonSubmit_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl___CustomNav0_StepNextButtonButton\"]";
				
				String registerTitle_XPath= "//*[@id=\"col_main_right\"]/h2"; 
				String loginTitle_Obtein;
				String loginTitle_Expected = "Register";
				
						
				//EncontaraElementoParaCargarlo
				registerPage = driver.findElement(By.xpath(registerPage_Xpath));
				registerPage.click();//DarClickAlElemento
				
				registerTitle = driver.findElement(By.xpath(registerTitle_XPath));//encuentra elemeto
				loginTitle_Obtein = registerTitle.getText();//para q obtenga el titulo
				//Asert=validar q cumpla la condicion
				Assert.assertEquals(loginTitle_Expected, loginTitle_Obtein);
				
				
			
				//Obtener el WebElement correspondinet al textfield firstname
				first_Name = driver.findElement(By.cssSelector(first_Name_Selector));
				first_Name.click();
				//Ingresar valores en el textfield firstname
				first_Name.sendKeys("Martha");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield lasttname
				lastName = driver.findElement(By.xpath(lastName_XPath));
				lastName.click();
				//Ingresar valores en el textfield firstname
				lastName.sendKeys("leon");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield email
				email = driver.findElement(By.xpath(email_Xpath));
				email.click();
				//Ingresar valores en el textfield email
				email.sendKeys("ma1@gmail.com");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield username
				userName = driver.findElement(By.xpath(userName_Xpath));
				userName.click();
				//Ingresar valores en el textfield username
				userName.sendKeys("Maleon");
				waitSeconds (driver, 1);
				
				
				//Obtener el WebElement correspondinet al textfield password
				password = driver.findElement(By.xpath(password_Xpath));
				password.click();
				//Ingresar valores en el textfield password
				password.sendKeys("Ml12345*");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield confirm password
				confirmPassword = driver.findElement(By.xpath(confirmPassword_Xpath));
				confirmPassword.click();
				//Ingresar valores en el textfield confirm pasword
				confirmPassword.sendKeys("Ml1234*");
				waitSeconds (driver, 1);
				
				
				//Obtener el WebElement correspondinet al textfield security question
				securityQuestion = driver.findElement(By.xpath(securityQuestion_Xpath));
				securityQuestion.click();
				//Ingresar valores en el textfield confirm pasword
				securityQuestion.sendKeys("nombre mascota");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield security ansuer
				securityAnswer = driver.findElement(By.xpath(securityAnswer_Xpath));
				securityAnswer.click();
				//Ingresar valores en el textfield confirm pasword
				securityAnswer.sendKeys("kyra");
				waitSeconds (driver, 1);
				

				//Obtener el WebElement correspondinet al boton submit
				buttonSubmit = driver.findElement(By.xpath(buttonSubmit_Xpath));
				buttonSubmit.click();
				waitSeconds (driver, 3);
				
				
			}//fin_metodo
	
			//***************************************************************************************************************************
			
			
			@Test(description="ErrorMessage_IsDisplayed_sendDifferentData_ConfirmPassword:"
					+ " //tc-14-Verify an error message is displayed when sending a different password on 'Password' and 'Confirm Password' fields")
			public void Test8() {
			
				
				WebElement registerTitle;
				WebElement registerPage;
				
				WebElement first_Name;
				WebElement lastName;	
				WebElement email;
				WebElement userName;
				WebElement password;
				WebElement confirmPassword;
				WebElement securityQuestion;
				WebElement securityAnswer;
				WebElement buttonSubmit;
				
					
				
				//hace_unaVariableParaQueGuardeLaVariableXpath
				String registerPage_Xpath = "//*[@id=\"ctl00_LoginView_RegisterLink\"]";
				String first_Name_Selector = "#ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_FirstName";
				
				String lastName_XPath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_LastName\"]";
				String email_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Email\"]"; 
				String userName_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_UserName\"]";
				String password_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Password\"]";
				String confirmPassword_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_ConfirmPassword\"]";
				String securityQuestion_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Question\"]";
				String securityAnswer_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl_CreateUserStepContainer_Answer\"]";
				String buttonSubmit_Xpath = "//*[@id=\"ctl00_Main_CreateUserWizardControl___CustomNav0_StepNextButtonButton\"]";
				
				String registerTitle_XPath= "//*[@id=\"col_main_right\"]/h2"; 
				String loginTitle_Obtein;
				String loginTitle_Expected = "Register";
				
						
				//EncontaraElementoParaCargarlo
				registerPage = driver.findElement(By.xpath(registerPage_Xpath));
				registerPage.click();//DarClickAlElemento
				
				registerTitle = driver.findElement(By.xpath(registerTitle_XPath));//encuentra elemeto
				loginTitle_Obtein = registerTitle.getText();//para q obtenga el titulo
				//Asert=validar q cumpla la condicion
				Assert.assertEquals(loginTitle_Expected, loginTitle_Obtein);
				
				
			
				//Obtener el WebElement correspondinet al textfield firstname
				first_Name = driver.findElement(By.cssSelector(first_Name_Selector));
				first_Name.click();
				//Ingresar valores en el textfield firstname
				first_Name.sendKeys(" ");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield lasttname
				lastName = driver.findElement(By.xpath(lastName_XPath));
				lastName.click();
				//Ingresar valores en el textfield firstname
				lastName.sendKeys("leon");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield email
				email = driver.findElement(By.xpath(email_Xpath));
				email.click();
				//Ingresar valores en el textfield email
				email.sendKeys(" ");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield username
				userName = driver.findElement(By.xpath(userName_Xpath));
				userName.click();
				//Ingresar valores en el textfield username
				userName.sendKeys("Maleon");
				waitSeconds (driver, 1);
				
				
				//Obtener el WebElement correspondinet al textfield password
				password = driver.findElement(By.xpath(password_Xpath));
				password.click();
				//Ingresar valores en el textfield password
				password.sendKeys(" ");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield confirm password
				confirmPassword = driver.findElement(By.xpath(confirmPassword_Xpath));
				confirmPassword.click();
				//Ingresar valores en el textfield confirm pasword
				confirmPassword.sendKeys(" ");
				waitSeconds (driver, 1);
				
				
				//Obtener el WebElement correspondinet al textfield security question
				securityQuestion = driver.findElement(By.xpath(securityQuestion_Xpath));
				securityQuestion.click();
				//Ingresar valores en el textfield confirm pasword
				securityQuestion.sendKeys(" ");
				waitSeconds (driver, 1);
				
				
				
				//Obtener el WebElement correspondinet al textfield security ansuer
				securityAnswer = driver.findElement(By.xpath(securityAnswer_Xpath));
				securityAnswer.click();
				//Ingresar valores en el textfield confirm pasword
				securityAnswer.sendKeys("kyra");
				waitSeconds (driver, 1);
				

				//Obtener el WebElement correspondinet al boton submit
				buttonSubmit = driver.findElement(By.xpath(buttonSubmit_Xpath));
				buttonSubmit.click();
				waitSeconds (driver, 3);
				
				
			}//fin_metodo
			
		
		
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		

		@AfterTest
		public void exit() {
			driver.quit();
		}//fin_metodo
		
		
		
		
		
		//Metodo para espera
		   public void waitSeconds(WebDriver driver, int seconds){
			      synchronized(driver){
			         try {
			            driver.wait(seconds * 1000);
			         } catch (InterruptedException e) {
			            // TODO Auto-generated catch block
			            //e.printStackTrace();
			         }
			      }
			   }

}//Fin Class


